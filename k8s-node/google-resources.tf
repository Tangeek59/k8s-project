provider "google" {
  credentials = "${file("../account.json")}"
  project     = "${var.project_name}"
  region      = "${var.region}"
}

resource "google_compute_instance" "k8s-master" {
  name         = "${var.name}"
  machine_type = "n1-standard-2"
  zone         = "${var.zone}"

  tags = ["${var.name}"]

  boot_disk {
    initialize_params {
      image = "projects/ubuntu-os-cloud/global/images/ubuntu-1804-bionic-v20181222"
      type = "pd-ssd"
    }
  }

  network_interface {
    subnetwork = "k8s-network-${var.region}"
    access_config { }
  }

  scheduling {
    automatic_restart = "false"
    preemptible= "true"
  }

  allow_stopping_for_update = "true"

  provisioner "remote-exec" {
    inline = ["sudo apt-get install -y python"]
    
    connection {
      type = "ssh"
      user = "${var.ssh_user}"
      private_key = "${file("~/.ssh/id_rsa")}"
    }
  }

  provisioner "local-exec" {
    command = "ansible-playbook -u ${var.ssh_user} -i '${google_compute_instance.k8s-master.network_interface.0.access_config.0.nat_ip},' install-tools.yaml"
  } 

}
