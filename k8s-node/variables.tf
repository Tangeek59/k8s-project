variable "project_name" {
  default = "certif-k8s"
}
variable "region" {
  default = "europe-west1"
}
variable "name" {
  default = "k8s-master"
}
variable "zone" {
  default = "europe-west1-b"
}
variable "ssh_user" {}
