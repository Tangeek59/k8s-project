variable "project_name" {}

variable "region" {
  default = "europe-west1"
}
variable "name" {
  default = "k8s-master"
}
variable "zone" {
  default = "europe-west1-b"
}
variable "ssh_user" {}

variable "network_name" {
  default  = "k8s-network"
}

variable "master_node_name" {
  default = "k8s-master"
}